'''
Created on Jul 18, 2014

@author: dimitrijer
'''

from PyQt4.Qt import QAbstractTableModel, QVariant, Qt, SIGNAL
from PyQt4 import QtCore

from sensei_util.log.Logger import Logger

from sensor.Sensor import AbstractSensor
import sensor.SensorModel as SensorModel
from sensor.SensorModel import NewSensorEvent, SensorUpdatedEvent, ClearEvent


class SensorTableModel(
        QAbstractTableModel, SensorModel.SensorEventListener, Logger):

    '''
    Implementation of QAbstractTableModel for data table showing
    various sensor information.
    '''

    UID, TYPE, READINGS, MIN, MAX, AVG, LAST, START = 0, 1, 2, 3, 4, 5, 6, 7
    '''Columns identifiers.'''

    header = {
        UID: 'UID',
        TYPE: 'Type',
        READINGS: 'Readings',
        MIN: 'Min',
        MAX: 'Max',
        AVG: 'Avg',
        LAST: 'Last',
        START: 'Started'
    }
    '''Header column names.'''

    columns = {
        UID: lambda sensor: sensor.uid,
        TYPE: lambda sensor: AbstractSensor.type_to_str(sensor.type),
        READINGS: lambda sensor: len(sensor),
        MIN: lambda sensor: '%.2f' % sensor.get_min(),
        MAX: lambda sensor: '%.2f' % sensor.get_max(),
        AVG: lambda sensor: '%.2f' % sensor.get_avg(),
        LAST: lambda sensor: '%.2f' % sensor.get_last_reading().data
        if sensor.get_last_reading() is not None else '',
        START: lambda sensor:
        sensor.start_timestamp.strftime('%d/%m/%Y %H:%M:%S')
    }
    '''Column mapping functions.'''

    def __init__(self, parent=None, *args):
        QAbstractTableModel.__init__(self, parent, *args)
        Logger.__init__(self)

        # subscribe to model events
        SensorModel.subscribe(self)

        self._sensor_ord_by_uid = dict()
        self._sensor_uid_by_ord = []
        '''Maps sensor ordinal number in table to sensor UID and vice versa.'''

    def handle_event(self, event):
        if isinstance(event, NewSensorEvent):
            _ord = len(self._sensor_uid_by_ord)
            self._sensor_uid_by_ord.append(event.sensor.uid)
            self._sensor_ord_by_uid[event.sensor.uid] = _ord
            self.debug('Added new sensor: %s' % str(event.sensor))

            # update view
            self.emitLayoutChanged()
        elif isinstance(event, SensorUpdatedEvent):
            # update data for changed sensor only
            self.emitDataChanged(event.sensor)
        elif isinstance(event, ClearEvent):
            # clear maps
            self._sensor_ord_by_uid = dict()
            self._sensor_uid_by_ord = []

            # update view
            self.emitLayoutChanged()

    def rowCount(self, parent=QtCore.QModelIndex()):
        return SensorModel.get_sensor_count()

    def columnCount(self, parent=QtCore.QModelIndex()):
        return len(SensorTableModel.header)

    def headerData(self, section, orientation, role=Qt.DisplayRole):
        if role == Qt.DisplayRole:
            if orientation == Qt.Horizontal:
                return SensorTableModel.header[section]

        return QVariant()

    def flags(self, index):
        return QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable

    def data(self, index, role):
        if not index.isValid():
            return QVariant()
        elif role != Qt.DisplayRole:
            return QVariant()

        # get the sensor
        sensor = SensorModel.get_sensor(self._sensor_uid_by_ord[index.row()])

        # call appropriate function
        return str(SensorTableModel.columns[index.column()](sensor))

    def emitLayoutChanged(self):
        self.emit(SIGNAL('layoutChanged()'))

    def emitDataChanged(self, sensor):
        row = self._sensor_ord_by_uid[sensor.uid]
        self.dataChanged.emit(
            self.index(
                row, 0), self.index(
                row, len(
                    SensorTableModel.columns) - 1))

    def get_sensor_by_ord(self, _ord):
        return SensorModel.get_sensor(self._sensor_uid_by_ord[_ord])
