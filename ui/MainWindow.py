'''
Created on Apr 13, 2014

@author: dimitrijer
'''

from PyQt4 import QtGui
from PyQt4.Qt import QAbstractItemView, pyqtSlot, SIGNAL,\
    QItemSelection, QPixmap, QLabel, Qt, pyqtSignal, QFileDialog, QTimer,\
    SLOT

from canvas import SensorCanvas, MplToolbox
from ui.generated import MainWindowUI

import os
from SensorTableModel import SensorTableModel
from sensor import SensorModel
from sensor.SensorModel import SensorEventListener, SensorUpdatedEvent,\
    ClearEvent
from ui.PlotSettings import PlotSettings


def emit_data_received():
    MainWindow.W.dataReceived.emit()


def init_gui():
    MainWindow.W = MainWindow()


class MainWindow(
        QtGui.QMainWindow, MainWindowUI.Ui_MainWindow, SensorEventListener):

    W = None
    '''The main instance.'''

    def __init__(self):
        super(MainWindow, self).__init__()

        # first setup the ui
        self.setupUi(self)

        # next, create main plotting canvas
        self.plotCanvas = SensorCanvas(
            self.centralwidget,
            self.horizontalScrollBar)
        self.plotCanvas.setObjectName(u'plotCanvas')
        self.verticalLayout_2.insertWidget(0, self.plotCanvas)

        # add its toolbar
        self.addToolBar(MplToolbox(self.plotCanvas, self))

        # load up three leds
        green_led = QPixmap()
        green_led.load(os.path.join(os.getcwd(), 'resources', 'green_led.png'))
        self._green_led = green_led.scaled(20, 20, Qt.KeepAspectRatio)

        red_led = QPixmap()
        red_led.load(os.path.join(os.getcwd(), 'resources', 'red_led.png'))
        self._red_led = red_led.scaled(20, 20, Qt.KeepAspectRatio)

        yellow_led = QPixmap()
        yellow_led.load(
            os.path.join(
                os.getcwd(),
                'resources',
                'yellow_led.png'))
        self._yellow_led = yellow_led.scaled(20, 20, Qt.KeepAspectRatio)

        self.connection_status = QLabel()
        self.connection_status.setPixmap(self._red_led)
        self.statusBar().addPermanentWidget(self.connection_status)
        self.is_connected = False

        # create table model
        self._sensor_table_model = SensorTableModel(self)

        # connect table view to model
        self.tableView.setModel(self._sensor_table_model)
        self.tableView.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.tableView.verticalHeader().hide()
        self.tableView.horizontalHeader().setStretchLastSection(True)
        self.tableView.setShowGrid(False)

        # subscribe to sensor model changes
        SensorModel.subscribe(self)

        # connect select event to according slot
        self.connect(self.tableView.selectionModel(),
                     SIGNAL(
            'selectionChanged(const QItemSelection&, const QItemSelection&)'),
            self.on_select)

        # connect slider signal to corresponding slot
        self.connect(self.horizontalScrollBar, SIGNAL('valueChanged(int)'),
                     self.on_slider_change)

        # connect exit menu to quit
        self.connect(self.actionExit, SIGNAL('triggered()'),
                     self.close)

        self.connect(self.actionSettings, SIGNAL('triggered()'),
                     self.get_plot_settings)

        # connect lock checkbox
        self.connect(self.checkLock, SIGNAL('stateChanged(int)'),
                     self.lock_graph)

        self.connect(self.pushSave, SIGNAL('clicked()'),
                     self.dump_sensor)

        self.connect(self.pushClear, SIGNAL('clicked()'),
                     self.on_clear)

        self.connect(self, SIGNAL('dataReceived()'),
                     self.on_data_received)

        self.show()

    dataReceived = pyqtSignal()

    @pyqtSlot(int)
    def lock_graph(self, state):
        is_locked = state == Qt.Checked
        self.plotCanvas.set_locked(is_locked)
        self.actionSettings.setEnabled(not is_locked)

    @pyqtSlot(QItemSelection, QItemSelection)
    def on_select(self, selected, deselected):
        if len(selected.indexes()) > 0:
            s = self._sensor_table_model.get_sensor_by_ord(
                selected.indexes()[0].row())
            self.replot(s)

            # enable dump button
            self.pushSave.setEnabled(True)

            self.actionSettings.setEnabled(True)
        else:
            self.pushSave.setEnabled(False)
            self.actionSettings.setEnabled(False)

    @pyqtSlot(int)
    def on_slider_change(self, change):
        if self.horizontalScrollBar.isEnabled():
            self.plotCanvas.replot(position=change)

    def replot(self, sensor):
        self.plotCanvas.replot(new_sensor=sensor)
        if self.plotCanvas.isHidden():
            # first remove vertical spacer
            self.verticalLayout_2.removeItem(self.verticalLayout_2.itemAt(1))
            self.plotCanvas.show()

    @pyqtSlot()
    def get_plot_settings(self):
        settings_dialog = PlotSettings(self, self.plotCanvas.settings)
        if settings_dialog.exec_():
            self.plotCanvas.set_settings(settings_dialog.settings)

    @pyqtSlot()
    def dump_sensor(self):
        s = self.plotCanvas.get_sensor()
        proposed_filename = os.path.join(os.getcwd(), s.uid + '.csv')
        _filter = 'Sensor Data (*.csv)'
        filename = QFileDialog.getSaveFileName(parent=self,
                                               caption='Save Sensor Data',
                                               directory=proposed_filename,
                                               filter=_filter,
                                               selectedFilter=_filter)

        # dump current sensor data to that file
        if not filename.isEmpty():
            self.plotCanvas.sensor.dump(filename)

    @pyqtSlot()
    def reset_led(self):
        self.connection_status.setPixmap(self._green_led)

    @pyqtSlot()
    def on_data_received(self):

        if not self.is_connected:
            # we're up and running
            self.is_connected = True
            self.connection_status.setPixmap(self._green_led)
        else:
            # blink the led
            self.connection_status.setPixmap(self._yellow_led)
            QTimer.singleShot(120, self, SLOT('reset_led()'))

        if not self.tableView.isEnabled():
            self.tableView.setEnabled(True)

        if not self.pushClear.isEnabled():
            self.pushClear.setEnabled(True)

    def handle_event(self, event):
        if isinstance(event, SensorUpdatedEvent):
            s = self.plotCanvas.get_sensor()
            if s is not None and event.sensor == s:
                self.replot(event.sensor)
        elif isinstance(event, ClearEvent):
            # clear the plot
            self.plotCanvas.clear_plot()
            self.plotCanvas.draw()

            self.tableView.clearSelection()
            self.tableView.setEnabled(False)
            self.pushClear.setEnabled(False)
            self.pushSave.setEnabled(False)
            self.actionSettings.setEnabled(False)

    @pyqtSlot()
    def on_clear(self):
        # clear the model
        SensorModel.clear()
