#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""seito_server.py: Seito is Sensei client that collects readings and sends
them to Sensei.

Seito should be run as a service on Pi."""

__author__    = "Dimitrije Radojević"
__copyright__ = "Copyright 2014 Dimitrije Radojević"
__email__     = "templaryum@gmail.com"

import logging, logging.handlers
from threading import Thread, Lock
from Queue import Queue
import os
import sys
import traceback
import requests
import time, datetime
import socket

# import generated protobuf module
from generated.SensorDefinition_pb2 import InitSessionRequest, \
InitSessionResponse, Sensor, PostReadingRequest, InitSensorResponse
from generated.ReadingDefinition_pb2 import Reading, DebugLog

shutdown = False

		
class Sender(Thread):
    """Thread that takes jobs from the queue and sends data to Sensei."""
    
    def __init__(self):
	Thread.__init__(None, self, 'Sender')
	
	# Construct url
	global CONFIG
	self.url = 'http://' + CONFIG['SENSEI_HOST'] + ':' + \
		    CONFIG['SENSEI_PORT'] + CONFIG['POST_READINGS_URL']
	
    def run():
	global CONFIG
	global READINGS
	
	# Block until next reading is available
	reading = READINGS.get()
	
	# Send it out to Sensei
	try:
	    r = requests.post(self.url, data=reading.SerializeToString(),
			      timeout=CONFIG['REQUEST_TIMEOUT_SECONDS'])
	    #if r.status 
	except Exception:
	    pass
    
def init():
	"""Initialize and configure Seito."""
	
	# Configuration
	global CONFIG
	CONFIG = dict(
	    SEITO_PORT = 5001, 						# Seito server port
	    SENSEI_HOST ='localhost',				# Host of Sensei server
	    SENSEI_PORT ='5000',					# Port of Sensei server
	    REQUEST_TIMEOUT_SECONDS = 10000,		# Reading req. timeout
	    INIT_SESSION_URL = '/sensor/init',		# Init session URL
	    POST_READINGS_URL = '/sensor/reading',	# Send readings URL
	)
	
	# Queue for reading packages, ready to be sent out
	global READINGS
	READINGS = Queue()
	
	# Initialize logging
	init_logging()

    
def init_server():
    """Starts a server socket that listens to incoming sensor connections."""
    global CONFIG
    global shutdown
    
    serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    serversocket.bind(('0.0.0.0', CONFIG['SEITO_PORT']))
    serversocket.listen(5)
    log.debug("Started server on %s" % CONFIG['SEITO_PORT'])
    try:
	while True:
	    # accept connections from outside
	    (clientsocket, address) = serversocket.accept()
	    reader = SensorReader(clientsocket)
	    reader.start()
	    log.debug(address)
    except KeyboardInterrupt:
	log.error("Caught exception!")
	shutdown = True
	return

    
def flush_to_server():
	"""Flushes aggregated data to Sensei server."""
	# Construct payload
	pass

if __name__ == '__main__':
    # Initialize
    init()
    
    # TODO Init sessions
    
    # Listen for incoming sensor connections
    init_server()
#    
#request = InitSessionRequest()
#
## add a couple of sensors
#sensor1 = request.sensors.add()
#sensor1.uid = "20.OBO343647575"
#sensor1.type = Sensor.TEMPERATURE
#sensor2 = request.sensors.add()
#sensor2.uid = "AC1-phase1"
#sensor2.type = Sensor.CURRENT
#
## describe
#request.sessionDescription = "Test session"
#
#r = requests.post("http://localhost:5000/sensor/init",
#				  data=request.SerializeToString())
#
#response = InitSessionResponse()
#try:
#	response.ParseFromString(r.content)
#except:
#	print 'Failed to parse proto!'
#	traceback.print_exc(file=sys.stdout)
#	sys.exit(1)
#
#print 'Session id: ', response.sessionId
#sessionId = response.sessionId
#sensors = {}
#for sensor in response.sensors:
#	print 'Sensor:', sensor.uid, 'id:', sensor.id
#	sensors[sensor.uid] = sensor.id
#
#while True:
#	# number of readings to accumulate prior to sending them
#	readingNumber = random.randint(1, 10)
#	readings = []
#	
#	print 'Gathering', readingNumber, 'readings...'
#	
#	while len(readings) < readingNumber:
#		
#		# Sleep for some time
#		time.sleep(10)
#		
#		# Assemble a reading from temperature sensor
#		reading = Reading()
#		reading.sensorId = sensors["20.OBO343647575"]
#		reading.timestamp = int(round(time.time() * 1000))
#		reading.reading = random.uniform(15, 35)
#		
#		readings.append(reading)
#	
#	# Send out the readings
#	request = PostReadingRequest()
#	request.sessionId = sessionId
#	request.readings.extend(readings)
#	
#	r = requests.post("http://localhost:5000/sensor/reading", data=request.SerializeToString())
#	print 'Posted %d readings to sensei.' % (readingNumber)
