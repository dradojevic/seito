# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'PlotSettings.ui'
#
# Created: Sun Jul 20 01:46:09 2014
#      by: PyQt4 UI code generator 4.10.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_PlotSettings(object):
    def setupUi(self, PlotSettings):
        PlotSettings.setObjectName(_fromUtf8("PlotSettings"))
        PlotSettings.setWindowModality(QtCore.Qt.ApplicationModal)
        PlotSettings.resize(400, 124)
        self.verticalLayout_4 = QtGui.QVBoxLayout(PlotSettings)
        self.verticalLayout_4.setObjectName(_fromUtf8("verticalLayout_4"))
        self.verticalLayout_3 = QtGui.QVBoxLayout()
        self.verticalLayout_3.setObjectName(_fromUtf8("verticalLayout_3"))
        self.lblRange = QtGui.QLabel(PlotSettings)
        self.lblRange.setObjectName(_fromUtf8("lblRange"))
        self.verticalLayout_3.addWidget(self.lblRange)
        self.horizontalSlider = QtGui.QSlider(PlotSettings)
        self.horizontalSlider.setMinimum(0)
        self.horizontalSlider.setMaximum(7)
        self.horizontalSlider.setPageStep(2)
        self.horizontalSlider.setProperty("value", 4)
        self.horizontalSlider.setTracking(True)
        self.horizontalSlider.setOrientation(QtCore.Qt.Horizontal)
        self.horizontalSlider.setTickPosition(QtGui.QSlider.TicksBelow)
        self.horizontalSlider.setTickInterval(1)
        self.horizontalSlider.setObjectName(_fromUtf8("horizontalSlider"))
        self.verticalLayout_3.addWidget(self.horizontalSlider)
        self.lblValue = QtGui.QLabel(PlotSettings)
        self.lblValue.setAlignment(QtCore.Qt.AlignCenter)
        self.lblValue.setObjectName(_fromUtf8("lblValue"))
        self.verticalLayout_3.addWidget(self.lblValue)
        spacerItem = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout_3.addItem(spacerItem)
        self.buttonBox = QtGui.QDialogButtonBox(PlotSettings)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName(_fromUtf8("buttonBox"))
        self.verticalLayout_3.addWidget(self.buttonBox)
        self.verticalLayout_4.addLayout(self.verticalLayout_3)

        self.retranslateUi(PlotSettings)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("accepted()")), PlotSettings.accept)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("rejected()")), PlotSettings.reject)
        QtCore.QMetaObject.connectSlotsByName(PlotSettings)

    def retranslateUi(self, PlotSettings):
        PlotSettings.setWindowTitle(_translate("PlotSettings", "Plot settings", None))
        self.lblRange.setText(_translate("PlotSettings", "Time range", None))
        self.lblValue.setText(_translate("PlotSettings", "24 hours", None))

