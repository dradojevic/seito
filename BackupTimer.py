'''
Created on Jul 26, 2014

@author: User
'''
import os
import threading

from sensei_util.log.Logger import Logger
from sensor import SensorModel
from sensor.Sensor import CompositeSensor
import datetime


class BackupTimer(Logger):

    '''
    Thread that performs sensor model backup in regular time intervals.
    '''

    _INTERVAL_MINUTES = 15
    '''Backup interval in minutes.'''

    _BACKUP_DIR = 'backup'

    def __init__(self):
        super(BackupTimer, self).__init__()

        if not os.path.exists(BackupTimer._BACKUP_DIR):
            os.makedirs(BackupTimer._BACKUP_DIR)

    def backup(self):
        '''
        Performs sensor model backup.
        '''
        try:
            self.debug("Performing backup...")
            for sensor in SensorModel.get_sensors():
                # skip composite sensors
                if not isinstance(sensor, CompositeSensor):
                    first_reading = sensor.get_first_reading()
                    filename = '.'.join((sensor.uid,
                                         first_reading.timestamp.strftime(
                                             '%d-%m-%Y.%H-%M-%S'),
                                         'csv'))
                    sensor.save(
                        os.path.join(
                            BackupTimer._BACKUP_DIR,
                            filename))

            self.debug('Sensor model saved.')
        except Exception:
            self.error('Model backup failed!', exc_info=1)

        # schedule next execution
        self._schedule_next()

    def _schedule_next(self):
        self._timer = threading.Timer(interval=BackupTimer._INTERVAL_MINUTES *
                                      60,
                                      function=self.backup)
        self._timer.setDaemon(True)
        self._timer.start()
        self.debug(
            'Next backup at %s.',
            self._get_next_backup_time().strftime("%d-%m-%Y %H:%M:%S"))

    def start(self):
        self._schedule_next()

    def _get_next_backup_time(self):
        return datetime.datetime.now() + \
            datetime.timedelta(minutes=BackupTimer._INTERVAL_MINUTES)
