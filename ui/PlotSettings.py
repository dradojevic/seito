'''
Created on Jul 19, 2014

@author: dimitrijer
'''
from PyQt4 import QtGui
from generated import PlotSettingsUI
from PyQt4.Qt import SIGNAL, pyqtSlot
import copy
from datetime import timedelta
from PyQt4.QtGui import QDialogButtonBox


class PlotSettings(QtGui.QDialog, PlotSettingsUI.Ui_PlotSettings):

    VALUE_INTERVAL_MAP = {
        0: timedelta(minutes=2),
        1: timedelta(minutes=30),
        2: timedelta(hours=1),
        3: timedelta(hours=3),
        4: timedelta(hours=6),
        5: timedelta(hours=12),
        6: timedelta(days=1),
        7: timedelta(days=3)
    }
    '''Maps values on range slider to time intervals.'''

    INTERVAL_VALUE_MAP = {v: k for k, v in VALUE_INTERVAL_MAP.items()}
    '''Reverse lookup for intervals.'''

    def __init__(self, main_window, settings):
        super(PlotSettings, self).__init__(parent=main_window)

        self.setupUi(self)
        self.settings = copy.copy(settings)

        initial_position = PlotSettings.INTERVAL_VALUE_MAP[
            timedelta(
                seconds=self.settings.get_range_seconds())]
        self.horizontalSlider.setSliderPosition(initial_position)
        self.lblValue.setText(
            PlotSettings._generate_range_text(initial_position))

        self.connect(self.horizontalSlider, SIGNAL('sliderMoved(int)'),
                     self._on_slider_update)
        self.connect(self.horizontalSlider, SIGNAL('valueChanged(int)'),
                     self._on_value_changed)
        self.connect(self.buttonBox.button(QDialogButtonBox.Ok),
                     SIGNAL('clicked()'),
                     self._update_range)

    @staticmethod
    def _generate_range_text(position):
        '''
        Generates label text that describes interval on given slider
        position.
        '''
        interval = PlotSettings.VALUE_INTERVAL_MAP[position]
        seconds = interval.seconds + interval.days * 60 * 60 * 24
        days, remainder = divmod(seconds, 60 * 60 * 24)
        hours, remainder = divmod(remainder, 60 * 60)
        minutes = remainder // 60

        text = ''

        if days > 0:
            text += '%d days' % days

        if hours > 0:
            if len(text) > 0:
                text += ' '
            text += '%d hours' % hours
        if minutes > 0:
            if len(text) > 0:
                text += ' '
            text += '%d minutes' % minutes

        return text

    @pyqtSlot(int)
    def _on_slider_update(self, position):
        self.lblValue.setText(PlotSettings._generate_range_text(position))

    @pyqtSlot()
    def _update_range(self):
        self.settings.range = PlotSettings.VALUE_INTERVAL_MAP[
            self.horizontalSlider.value()]

    @pyqtSlot(int)
    def _on_value_changed(self, position):
        # update label here, too, because page changes aren't picked up in
        # slider update slot
        self.lblValue.setText(PlotSettings._generate_range_text(position))
