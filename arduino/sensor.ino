
/**
 * sensor.ino: Arduino sensing sensor sketch
 * 
 * This sketch measures current from three current sensors sensors
 * (CT1, CT2, CT3) and temperature from up to 10 OneWire sensors
 * connected in parasite mode, and sends that data to Seito.
 *
 */

/**
 * DEFINES
 */

#define DEBUG_INIT 0     // print debug to Serial while init
#define DEBUG_VERBOSE 0  // print A LOT of debug to Serial
#define DEBUG_PROTO 0    // print debug to Serial while send/recv
#define DEBUG_CONNECT 0  // print debug to Serial while connect120
#define FREE_MEM 0       // log free memory on start
#define PROTO_RECV 0     // proto receiving and decoding functionality                       
                         
#define MESSAGE_LENGTH_FIELD_LEN 4
#define HANDLER_CODE_FIELD_LEN 2
#define HEADER_LEN (MESSAGE_LENGTH_FIELD_LEN + HANDLER_CODE_FIELD_LEN)

#define CT1 1 // current sensor 1 enabled
#define CT2 1 // current sensor 2 enabled
#define CT3 1 // current sensor 3 enabled

/**
 * INCLUDES
 */

// Ethernet
#include <SPI.h>
#include <Ethernet.h>

// Protobuf
#include <pb_encode.h>
#include <pb_arduino_encode.h>
#include <ReadingDefinition.pb.h>

#if PROTO_RECV
#include <pb_decode.h>
#include <pb_arduino_decode.h>
#endif

// Temperature sensing
#include <OneWire.h>
#include <DallasTemperature.h>

// Emon lib (current sensing)
#if CT1 || CT2 || CT3
#include <EmonLib.h>
#endif
                         
/**
 * CONSTANTS
 */                         
                         
#define TEMP_INTERVAL_MILLIS 15000 // temperature reading/sending interval (15s)
#define IRMS_INTERVAL_MILLIS 1000 // current sending interval (1s)
#define ONE_WIRE_BUS 4            // DS18B20 Data wire digital pin (Dig4)
#define TEMPERATURE_PRECISION 9   // temperature reading precision (bits)
#define MAX_ONE_WIRE_DEVICES 10   // maximum number of OneWire devices
#define CURRENT_SAMPLES 1480      // number of samples to take when measuring
                                  // current
#define CALIBRATION_FACTOR 47.0 // for 18 ohms (for 15 it's 111.11)

#define LED_PIN 9

// Our interface's MAC address
byte mac[] = { 0x90, 0xA2, 0xDA, 0x0E, 0xD5, 0x38 };

// Our IP address
IPAddress ip(192,168,10,2);

// Seito's IP address
IPAddress seitoIp(192,168,10,1);

// Seito port
unsigned int seitoPort = 5001;

// The client we'll use to send stuff to seito
EthernetClient client;

// Energy monitor instances for each sensor
#if CT1
EnergyMonitor emon1;
float Irms1Accumulator = 0;
#endif

#if CT2
EnergyMonitor emon2;
float Irms2Accumulator = 0;
#endif

#if CT3
EnergyMonitor emon3;
float Irms3Accumulator = 0;
#endif

// Array to hold device addresses
DeviceAddress thermometers[MAX_ONE_WIRE_DEVICES];

// OneWire device count
unsigned int oneWireDevices;

// Current sensor count
unsigned int currentSensors = 0;

// Setup a oneWire instance to communicate with OneWire devices
OneWire oneWire(ONE_WIRE_BUS);

// Pass our oneWire reference to Dallas Temperature. 
DallasTemperature sensors(&oneWire);

// Buffer for encoding proto messages
uint8_t buffer[256];

// Connected flag
boolean isConnected = false;

// Time at which the last reading was sent
unsigned long lastTemperatureSentTime;
#if CT1 || CT2 || CT3
unsigned long lastCurrentSentTime;
unsigned int currentMeasuresTaken = 0;
#endif

typedef struct _ProtoData {
  unsigned int len;
  SensorReading **readings;
} ProtoData;

// callback for encoding string fields in proto
bool encodeString(pb_ostream_t* ostream, const pb_field_t* field, void* const* arg) {
  char *str = (char*) *arg;
  if (!pb_encode_tag_for_field(ostream, field))
    return false;

  return pb_encode_string(ostream, (uint8_t*)str, strlen(str));
}

// callback for encoding repeated readings
bool encodeReadings(pb_ostream_t* ostream, const pb_field_t* field, void* const* arg) {
  
  ProtoData* request = (ProtoData*) *arg;
  
  for (unsigned int i = 0; i < request->len; i++) {
    if (!pb_encode_tag_for_field(ostream, field)) {
      return false;
    }
    
    if (!pb_encode_submessage(ostream, SensorReading_fields, request->readings[i])) {
      return false;
    }
  }
  
  return true;
}
  
// Converts OneWire sensor address to string
void addressToString(char* dst, uint8_t* deviceAddress)
{
  int counter = 0;
  for (uint8_t i = 0; i < 8; i++)
  {
    // zero pad the address if necessary
    if (deviceAddress[i] < 16) {
      dst[counter++] = '0';
    }
    
    String hexa = String(deviceAddress[i], HEX);
    for (uint8_t j = 0; j < hexa.length(); j++) {
      dst[counter++] = hexa[j];
    }
  }
  
  dst[counter] = '\0';
}

// Sends a proto message, defined with fields
void sendMessage(short handlerCode, const pb_field_t fields[], void* msg) {

  // wrap output buffer in ostream
  pb_ostream_t ostream = pb_ostream_from_buffer(buffer + HEADER_LEN, sizeof(buffer) - HEADER_LEN);
  
  // encode message
  if (pb_encode(&ostream, fields, msg)) {
    #if DEBUG_PROTO
    Serial.print("Encoded ");
    Serial.print(ostream.bytes_written, DEC);
    Serial.println(" bytes.");
    #endif
    
    // set message len and handler code (network order
    // = big endian)
    buffer[0] = ostream.bytes_written >> 24;
    buffer[1] = ostream.bytes_written >> 16;
    buffer[2] = ostream.bytes_written >> 8;
    buffer[3] = ostream.bytes_written;

    buffer[4] = handlerCode >> 8;
    buffer[5] = handlerCode;
    
    // write to client
    int8_t bytesSent = client.write(buffer, ostream.bytes_written + HEADER_LEN);
    #if DEBUG_PROTO
    Serial.print("Sent ");
    Serial.print(bytesSent, DEC);
    Serial.println(" bytes.");
    #endif
  }
#if DEBUG_PROTO
  else {
    Serial.println("Encode failed!");
  }
#endif
}

#if PROTO_RECV
void receiveMessage(const pb_field_t fields[], void *dest) {
  // clear input buffer
  memset(buffer, 0, sizeof(buffer));
 
  // read into buffer   
  int bytesRead = client.read(buffer, sizeof(buffer));
  #if DEBUG_PROTO
  Serial.print("Read ");
  Serial.println(bytesRead, DEC);
  Serial.print(" bytes.");
  #endif
  
  // wrap istream around input buffer
  pb_istream_t istream = pb_istream_from_buffer(buffer, sizeof(buffer));
  
  // decode message
  #if DEBUG_PROTO
  if (pb_decode(&istream, fields, dest)) {
    Serial.println("Decode succeeded.");
  } else {
    Serial.println("Decode failed!");
  }
  #else
  pb_decode(&istream, fields, dest);
  #endif
  
}
#endif

// constructs a sensor reading proto
SensorReading* buildSensorReading(int ord, float data) {
    SensorReading *reading = (SensorReading*) malloc(sizeof(SensorReading));
    reading->reading = data;
    #if CT1
    if (ord == 0) {
      strcpy(reading->sensor.uid, "CT1");
      reading->sensor.type = SensorType_CURRENT;
      return reading;
    }
    #endif
    
    #if CT2
    if (ord == 1) {
      strcpy(reading->sensor.uid, "CT2");
      reading->sensor.type = SensorType_CURRENT;
      return reading;
    }
    #endif
    
    #if CT3
    if (ord == 2) {
      strcpy(reading->sensor.uid, "CT3");
      reading->sensor.type = SensorType_CURRENT;
      return reading;
    }
    #endif
    
    // this is a temperature sensor
    addressToString(reading->sensor.uid, thermometers[ord - 3]);
    reading->sensor.type = SensorType_TEMPERATURE;
    return reading;
}

// Attempts to connect to Seito; bloks until connected
void connectToSeito() {
 
  // turn off led
  digitalWrite(LED_PIN, LOW);
  
  // make sure client is stopped
  if (client) {
    client.stop();
  }
  
  // Try to connect to Seito
  while (!isConnected) {
 
    #if DEBUG_CONNECT
    Serial.print("Connecting to Seito at ");
    Serial.print(seitoIp);
    Serial.print(":");
    Serial.print(seitoPort);
    Serial.println("...");
    #endif
    
    if (client.connect(seitoIp, seitoPort)) {
      // Nice, we're connected
      isConnected = true;
      #if DEBUG_CONNECT
      Serial.println("Connected!");
      #endif
      
      // turn on led
      digitalWrite(LED_PIN, HIGH);
      return;
    }
    
    #if DEBUG_CONNECT
    Serial.println("Failed!");
    #endif
    
    unsigned long millisToSleep = 5000;
    unsigned long startTime = millis();
    while (millis() - startTime < millisToSleep) {
      // blink the led
      delay(100);
      digitalWrite(LED_PIN, HIGH);
      delay(100);
      digitalWrite(LED_PIN, LOW);
    }
  }
}

#if FREE_MEM

// found this in Arduino cookbook

// variables created by the build process when compiling the sketch
extern int __bss_end;
extern void *__brkval;

int memory_free()
{
    int free_value;
    if ((int)__brkval == 0)
        free_value = ((int)&free_value) - ((int)&__bss_end);
    else
        free_value = ((int)&free_value) - ((int)__brkval);
    return free_value;
}
#endif /* FREE_MEM */

void setup() {

  // Start serial communication and wait for port to open
  Serial.begin(9600);

  #if DEBUG_INIT
  Serial.println("Sensor starting...");
  #endif
  
  // Setup led pin
  pinMode(LED_PIN, OUTPUT);
  
  // Configure ethernet with our mac/IP
  Ethernet.begin(mac, ip);
  
  // Give the controller some time to initialize
  delay(1000);

  #if DEBUG_INIT
  Serial.println("Ethernet library set up.");
  #endif
  
  // Set up current monitoring on inputs
  #if CT1
  emon1.current(1, CALIBRATION_FACTOR);
  currentSensors++;
  #endif
  
  #if CT2
  emon2.current(2, CALIBRATION_FACTOR);
  currentSensors++;
  #endif
  
  #if CT3
  emon3.current(3, CALIBRATION_FACTOR);
  currentSensors++;
  #endif

  #if CT1 || CT2 || CT3
  #if DEBUG_INIT
  Serial.print("Emon library set up (");
  Serial.print(currentSensors, DEC);
  Serial.println(" sensors active).");
  #endif
  #endif
  
  // Start up the Dallas library
  sensors.begin();
  
  // locate devices on the bus
  oneWireDevices = sensors.getDeviceCount();

  // we only support so many devices!
  if(oneWireDevices > MAX_ONE_WIRE_DEVICES) {
    oneWireDevices = MAX_ONE_WIRE_DEVICES;
  }
  
  // get sensor addresses
  for (int i = 0; i < oneWireDevices; i++) {
    if(!sensors.getAddress(thermometers[i], i)) {
      #if DEBUG_INIT
      Serial.print("Failed to get address of device ");
      Serial.println(i, DEC);
      #endif
      // TODO mark it as failed somehow
    } else {
     
      // set the resolution to 9 bit
      sensors.setResolution(thermometers[i], TEMPERATURE_PRECISION);
    }
  }
  
  #if DEBUG_INIT
  Serial.print("OneWire library set up (");
  Serial.print(oneWireDevices, DEC);
  Serial.println(" devices detected).");
  #endif

  // Establish connection to Seito and send init request
  connectToSeito();
  
  #if DEBUG_INIT && FREE_MEM
  Serial.print("Free memory: ");
  Serial.print(memory_free(), DEC);
  Serial.println(" bytes");
  #endif
  
  // sensor is now started
  lastTemperatureSentTime = millis();
  #if CT1 || CT2 || CT3
  lastCurrentSentTime = millis();
  #endif
  
  #if DEBUG_INIT
  Serial.println("Sensor initialized successfully!");
  #endif
}

void loop() {  
  
  // measure current
  // TODO peak detection should use smaller sample number and
  // average in time
  #if CT1
  Irms1Accumulator += emon1.calcIrms(CURRENT_SAMPLES);
  #endif
  
  #if CT2
  Irms2Accumulator += emon2.calcIrms(CURRENT_SAMPLES);
  #endif
  
  #if CT3
  Irms3Accumulator += emon3.calcIrms(CURRENT_SAMPLES);
  #endif
  
  #if CT1 || CT2 || CT3
  // increase measures taken
  currentMeasuresTaken++;

  // Check if we need to send current  
  if (millis() - lastCurrentSentTime > IRMS_INTERVAL_MILLIS) {
    {
      // blink the led
      digitalWrite(LED_PIN, LOW);
     
      #if DEBUG_VERBOSE
      Serial.print("Took ");
      Serial.print(currentMeasuresTaken, DEC);
      Serial.println(" current measures.");
      #endif
      
      // prepare the readings
      ProtoData request;
      request.len = currentSensors;
      request.readings = (SensorReading**) malloc(currentSensors * sizeof(SensorReading*));
      unsigned int IrmsCount = 0;
      float averageIrms;
     
      #if CT1
      // calculate average
      averageIrms = Irms1Accumulator / currentMeasuresTaken;
      
      // clear accumulator
      Irms1Accumulator = 0;
      
      // set reading
      request.readings[IrmsCount] = buildSensorReading(0, averageIrms);
      
      #if DEBUG_VERBOSE
      Serial.println(request.readings[IrmsCount]->sensor.uid);
      #endif

      // increase counter
      IrmsCount++;      
      #endif /* CT1 */
      
       #if CT2
      // calculate average
      averageIrms = Irms2Accumulator / currentMeasuresTaken;
      
      // clear accumulator
      Irms2Accumulator = 0;
      
      // set reading
      request.readings[IrmsCount] = buildSensorReading(1, averageIrms);

      #if DEBUG_VERBOSE
      Serial.println(request.readings[IrmsCount]->sensor.uid);
      #endif
      
      // increase counter
      IrmsCount++;
      #endif /* CT2 */
      
      #if CT3
      // calculate average
      averageIrms = Irms3Accumulator / currentMeasuresTaken;
      
      // clear accumulator
      Irms3Accumulator = 0;
      
      // set reading
      request.readings[IrmsCount] = buildSensorReading(2, averageIrms);

      #if DEBUG_VERBOSE
      Serial.println(request.readings[IrmsCount]->sensor.uid);
      #endif
      
      // increase counter
      IrmsCount++;
      #endif /* CT3 */

      // prepare proto
      SendReadingsRequest currentReadings;
      currentReadings.readings.funcs.encode = encodeReadings;
      currentReadings.readings.arg = &request;
      
      // send out proto
      sendMessage(1, SendReadingsRequest_fields, &currentReadings);
      
      // clear memory
      for (int i = 0; i < currentSensors; i++) {
        free(request.readings[i]);
      }
      free(request.readings);
      
      // clear measures counter
      currentMeasuresTaken = 0;
      
      // reset sent time
      lastCurrentSentTime = millis();
      
      #if DEBUG_VERBOSE
      Serial.println("Done sending current.");
      #endif

      digitalWrite(LED_PIN, HIGH);
    }
  }
  #endif /* CT1 || CT2 || CT3 */

  // check if there are any OneWire devices connected
  if (oneWireDevices > 0) {
    
    // check if it's time to send temperature yet
    if (millis() - lastTemperatureSentTime > TEMP_INTERVAL_MILLIS) {
    
      // blink the led
      digitalWrite(LED_PIN, LOW);
      
      // request readings from OneWire sensors
      sensors.requestTemperatures();
  
      {
        // prepare readings
        ProtoData request;
        request.len = oneWireDevices;
        request.readings = (SensorReading**) malloc(oneWireDevices * sizeof(SensorReading*));
        
        // set temperature readings
        for (int i = 0; i < oneWireDevices; i++) {
          request.readings[i] = buildSensorReading(i + 3, sensors.getTempC(thermometers[i]));
        }
        
        // prepare proto
        SendReadingsRequest temperatureReadings;
        temperatureReadings.readings.funcs.encode = encodeReadings;
        temperatureReadings.readings.arg = &request;  

        // send out proto
        sendMessage(1, SendReadingsRequest_fields, &temperatureReadings);
        
        // clear memory
        for (int i = 0; i < oneWireDevices; i++) {
          free(request.readings[i]);
        }
        free(request.readings);
      }
    
      // reset last sent time
      lastTemperatureSentTime = millis();
      
      digitalWrite(LED_PIN, HIGH);
    }
  }
  
  // check connection
  if (!client.connected()) {
    
    // fail, we need to reconnect
    isConnected = false;
    connectToSeito();
  }
}
