'''
Created on Apr 13, 2014

@author: dimitrijer
'''

import socket
import time

from sensei_util.comm import SimpleProtocolParser
import random
from sensei_util.comm.Message import MessageWithHandlerCode
from sensei_protobuf.generated.ReadingDefinition_pb2 import DebugLog,\
    SendReadingsRequest, CURRENT

if __name__ == '__main__':
    parser = SimpleProtocolParser()
    try:
        sock = socket.socket()
        sock.settimeout(1)
        sock.connect(('localhost', 5001))
        print 'Connected!'

        dl = DebugLog()
        dl.message = 'Test'
        dlm = dl.SerializeToString()
        print 'Size : ', len(dlm)
        msg = parser.encode_message(
            MessageWithHandlerCode(
                0,
                dl.SerializeToString()))
        print 'Encoded size: ', len(msg)
        sock.sendall(msg)

        while True:

            request = SendReadingsRequest()
            for i in range(0, random.randrange(1, 3)):
                s = 'Sensor-' + str(i)
                reading = request.readings.add()
                reading.sensor.uid = s
                reading.sensor.type = CURRENT
                reading.reading = random.randrange(-10, 35)

            dlm = request.SerializeToString()
            print 'Size : ', len(dlm)
            msg = parser.encode_message(MessageWithHandlerCode(1, dlm))
            print 'Encoded size: ', len(msg)
            sock.sendall(msg)

            time.sleep(3)
    finally:
        sock.close()
