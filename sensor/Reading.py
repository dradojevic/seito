'''
Created on Jul 18, 2014

@author: dimitrijer
'''

from datetime import datetime


class Reading(object):

    '''
    Represents a single reading from a single sensor.
    '''

    def __init__(self, sensor_uid, data):
        self.sensor_uid = sensor_uid
        self.data = data
        self.timestamp = datetime.now()
