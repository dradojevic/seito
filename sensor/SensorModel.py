'''
This module contains all sensors in the system.

Created on Jul 18, 2014

@author: dimitrijer
'''

from sensor.Sensor import AbstractSensor, CompositeSensor, Sensor

_sensors = dict()
'''All sensors mapped by their UID.'''

_listeners = []
'''Listeners to model changes (i.e. new sensor appears, sensor data
updated etc.).'''

AC_SENSOR_UID = 'CTA'
'''UID used for composite current sensor.'''


class SensorEvent(object):

    '''
    Abstract sensor event.
    '''

    def __init__(self, sensor):
        self.sensor = sensor


class ClearEvent(SensorEvent):

    '''
    This event is triggered when clear button is pressed.
    '''

    def __init__(self):
        # No specific sensor
        super(ClearEvent, self).__init__(None)


class NewSensorEvent(SensorEvent):

    '''
    This event is triggered when a new sensor is added to the
    model.
    '''
    pass


class SensorUpdatedEvent(SensorEvent):

    '''
    This event is triggered when a sensor is updated with a new
    reading.
    '''

    def __init__(self, sensor, reading):
        super(SensorUpdatedEvent, self).__init__(sensor)
        self.reading = reading


class SensorEventListener(object):

    '''
    This interface should be implemented by all classes that need
    to listen to sensor events.
    '''

    def handle_event(self, event):
        raise NotImplementedError()


def subscribe(listener):
    '''
    Subscribe to sensor model changes.
    '''
    _listeners.append(listener)


def get_sensor(uid):
    '''
    Returns sensor with given UID, if such exists.
    '''
    return _sensors[uid] if uid in _sensors else None


def _add_sensor(sensor):
    _sensors[sensor.uid] = sensor

    for listener in _listeners:
        listener.handle_event(NewSensorEvent(sensor))


def create_sensor(uid, _type):
    '''
    Creates and adds non-existing sensor to the model and fires a
    NewSensorEvent.

    Has no effect if sensor already exists.
    '''
    sensor = get_sensor(uid)
    if sensor:
        return

    # add a new sensor
    sensor = Sensor(uid, _type)
    _add_sensor(sensor)

    # check if we need to create/update composite sensors
    if _type == AbstractSensor.CURRENT:
        add_composite = False
        composite = get_sensor(AC_SENSOR_UID)
        if composite is None:
            # create composite current sensor
            composite = CompositeSensor(AC_SENSOR_UID, _type)
            add_composite = True

        # add child to composite sensor
        composite.add_child(sensor)

        if add_composite:
            # add composite sensor to model (we delayed it because we needed
            # to add the child first)
            _add_sensor(composite)


def add_reading(reading, _type):
    '''
    Adds provided reading to specific sensor data and fires a
    SensorUpdatedEvent.
    '''
    sensor = get_sensor(reading.sensor_uid)
    sensor.append(reading)

    for listener in _listeners:
        listener.handle_event(SensorUpdatedEvent(sensor, reading))
        # notify listeners for parent, too
        if sensor.parent is not None:
            listener.handle_event(SensorUpdatedEvent(sensor.parent, reading))


def get_sensor_count():
    return len(_sensors)


def clear():
    '''
    Clears all data (sensors, readings...) from the model.
    '''

    _sensors.clear()

    for listener in _listeners:
        listener.handle_event(ClearEvent())


def get_sensors():
    return _sensors.values()
