'''
Holds classes that draw graphs.

Created on Apr 13, 2014

@author: dimitrijer
'''

from datetime import timedelta
import datetime
import calendar
import dateutil
from threading import RLock

from PyQt4 import QtGui
from matplotlib import dates
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas, \
    NavigationToolbar2QTAgg
from matplotlib.pyplot import legend
from matplotlib.ticker import NullLocator

import matplotlib.pyplot as plt
from sensei_protobuf.generated.ReadingDefinition_pb2 import TEMPERATURE, \
    CURRENT
from sensei_util.log import Logger


class MplToolbox(NavigationToolbar2QTAgg):

    '''
    Custom toolbox for Matplotlib, with unnecessary buttons removed.
    '''

    def _init_toolbar(self):
        super(MplToolbox, self)._init_toolbar()

        # remove unnecessary buttons
        # configure subplots
        self.removeAction(self._actions['configure_subplots'])
        self.removeAction(self.actions()[-2])  # figure options


class MplCanvas(FigureCanvas):

    '''
    Main graph canvas class.
    Ultimately, this is a QWidget (as well as a FigureCanvasAgg, etc.).
    '''

    def __init__(self, parent=None, width=5, height=4, dpi=100):
        fig = plt.figure(figsize=(width, height), dpi=dpi)
        super(MplCanvas, self).__init__(fig)

        # add one subplot to the first index
        self.axes = fig.add_subplot(111)

        # add bottom margin
        plt.subplots_adjust(bottom=0.25)

        self.setParent(parent)

        FigureCanvas.setSizePolicy(self,
                                   QtGui.QSizePolicy.Expanding,
                                   QtGui.QSizePolicy.Expanding)
        FigureCanvas.updateGeometry(self)

    def setup_plot(self):
        '''
        Set up common properties of axes (prepare the plot).
        '''
        # set margin for y axis (it will be auto-scaled)
        self.axes.set_ymargin(0.2)

        # we don't want the axes cleared every time plot() is called
        self.axes.hold(True)

        # show grid
        self.axes.grid(linestyle=':', linewidth=0.2)

        # disable auto-scaling for x, enable for y
        self.axes.set_autoscalex_on(False)
        self.axes.set_autoscaley_on(True)


class CanvasSettings(object):

    '''
    Holds plot settings that can be set by user.
    '''

    EVERY_SIX_HOURS = dates.HourLocator(interval=6)
    EVERY_TWO_HOURS = dates.HourLocator(interval=2)
    EVERY_HOUR = dates.HourLocator(interval=1)
    EVERY_30_MINUTES = dates.MinuteLocator(interval=30)
    EVERY_15_MINUTES = dates.MinuteLocator(interval=15)
    EVERY_5_MINUTES = dates.MinuteLocator(interval=5)
    EVERY_3_MINUTES = dates.MinuteLocator(interval=3)
    EVERY_MINUTE = dates.MinuteLocator(interval=1)
    EVERY_30_SECONDS = dates.SecondLocator(interval=30)
    '''A bunch of static locators, to avoid GC.'''

    def __init__(self, time_range=timedelta(days=1)):
        self.range = time_range

    def get_range_seconds(self):
        return self.range.days * 24 * 3600 + self.range.seconds

    def get_margin_seconds(self):
        # margin is ~5% of the range
        return int(0.05 * self.get_range_seconds())

    def get_slider_step_seconds(self):
        # slider step should be ~1% of range
        return int(0.01 * self.get_range_seconds())

    def get_slider_page_step_seconds(self):
        # about one margin
        return self.get_margin_seconds()

    def get_locators(self):
        days = self.range.days
        seconds = self.range.seconds + days * 60 * 60 * 24
        if days >= 1:
            # tick every six hours
            major_locator = CanvasSettings.EVERY_SIX_HOURS
            minor_locator = CanvasSettings.EVERY_HOUR
        elif seconds >= 6 * 60 * 60:
            # tick every two hours
            major_locator = CanvasSettings.EVERY_TWO_HOURS
            minor_locator = CanvasSettings.EVERY_30_MINUTES
        elif seconds >= 60 * 60:
            # tick every 15m
            major_locator = CanvasSettings.EVERY_15_MINUTES
            minor_locator = CanvasSettings.EVERY_3_MINUTES
        elif seconds >= 60 * 10:
            # tick every 5m
            major_locator = CanvasSettings.EVERY_5_MINUTES
            minor_locator = CanvasSettings.EVERY_MINUTE
        else:
            # tick every 1m
            major_locator = CanvasSettings.EVERY_MINUTE
            minor_locator = CanvasSettings.EVERY_30_SECONDS

        return major_locator, minor_locator

    def __str__(self):
        return 'Range: %ss (%s)\nMargin (page step): %ss\nSlider step: %ss' % \
            (self.get_range_seconds(), str(self.range),
             self.get_margin_seconds(), self.get_slider_step_seconds())


class SensorCanvas(MplCanvas):

    '''
    Graph plot for sensor data.
    '''

    def __init__(
            self, parent, scrollbar, sensor=None, width=5, height=5, dpi=100):
        self._sensor = sensor
        self.settings = CanvasSettings()
        super(SensorCanvas, self).__init__(parent, width, height, dpi)

        self._scrollbar = scrollbar
        self._last_scrollbar_position = None

        # this is an old style class, so we cannot inherit Logger
        self.log = Logger(self.__class__.__name__)
        self._is_locked = False
        self._replot_lock = RLock()

    def set_locked(self, locked):
        self._is_locked = locked

    def clear_plot(self, clear_sensor=True):
        with self._replot_lock:
            # clear axes
            self.axes.clear()
            self.lines = []
            self.axes.xaxis.set_major_locator(NullLocator())
            self.axes.xaxis.set_minor_locator(NullLocator())
            if clear_sensor:
                self._sensor = None
            self._last_scrollbar_position = None

    def _prepare_plot(self):
        self.axes.clear()
        self.setup_plot()

        # set title of the plot
        self.axes.set_title(self._sensor.uid)

        # set y axis labels
        if self._sensor.type == TEMPERATURE:
            self.axes.set_ylabel('Temperature [C]')
        elif self._sensor.type == CURRENT:
            self.axes.set_ylabel('Current [A]')

        # x axis ticks should be in this form
        self.axes.xaxis.set_major_formatter(dates.DateFormatter('%H:%M'))

        self._adjust_axes()
        self._adjust_ticks()

    def get_sensor(self):
        with self._replot_lock:
            return self._sensor

    def _adjust_axes(self, position=None):
        width = timedelta(seconds=self.settings.get_range_seconds())
        margin = timedelta(seconds=self.settings.get_margin_seconds())
        # set to true if we want to adjust plot axes
        adjust = True
        if position is None:

            # get first and last readings and use them as left/right limit
            first_ts = self._sensor.get_first_reading().timestamp
            last_ts = self._sensor.get_last_reading().timestamp

            first_left_x = dates.date2num(first_ts - margin)
            first_right_x = dates.date2num(first_ts + width)

            last_left_x = dates.date2num(last_ts - width)
            last_right_x = dates.date2num(last_ts + margin)

            # check if we can see both first and last datapoint in a single
            # graph
            use_scroll = first_right_x < last_right_x

            if use_scroll:
                # show range that includes last datapoint (first datapoint will
                # be out of range)
                left_x = last_left_x
                right_x = last_right_x

                # enable scroll bar
                if not self._scrollbar.isEnabled():
                    self._scrollbar.setEnabled(True)

                # scrollbar value will point to position of right x limit, so
                # set max scrollbar value to the rightmost value, min to the
                # leftmost value
                _max = last_ts + margin
                self._scrollbar.setMaximum(
                    int(calendar.timegm(_max.timetuple())))
                _min = first_ts + width
                self._scrollbar.setMinimum(
                    int(calendar.timegm(_min.timetuple())))

                # current value is all to the right (note: this will emit a
                # valueChanged signal)
                self._scrollbar.setValue(self._scrollbar.maximum())
                self._last_scrollbar_position = self._scrollbar.maximum()

                self._scrollbar.setSingleStep(
                    self.settings.get_slider_step_seconds())
                self._scrollbar.setPageStep(
                    self.settings.get_slider_page_step_seconds())
            else:
                # show range that includes both datapoints
                left_x = first_left_x
                right_x = first_right_x

                # disable scroll bar
                if self._scrollbar.isEnabled():
                    self._scrollbar.setEnabled(False)
                    self._scrollbar.setMaximum(0)
                    self._scrollbar.setMinimum(0)
                    self._scrollbar.setValue(0)

                # adjust only if we cannot see datapoints with current axes
                current_left_x, current_right_x = self.axes.get_xlim()
                if current_left_x == left_x and current_right_x == right_x:
                    adjust = False
        else:
            if position == self._last_scrollbar_position:
                return

            # set rightmost position from scroll bar
            dt = datetime.datetime.fromtimestamp(
                position,
                dateutil.tz.tzlocal())
            right_x = dates.date2num(dt)
            left_x = dates.date2num(dt - (width + margin))
            self._last_scrollbar_position = position

        if adjust:
            self.log.trace(
                'Setting range to: [%s, %s]' %
                (str(left_x), str(right_x)))
            self.axes.set_xlim((left_x, right_x))
            # traceback.print_stack()

            # y will be auto-adjusted

    def _adjust_ticks(self):

        # set locators and formatters for ticks
        major_locator, minor_locator = self.settings.get_locators()
        if major_locator is not self.axes.xaxis.get_major_locator():
            self.axes.xaxis.set_major_locator(major_locator)
            self.axes.xaxis.set_minor_locator(minor_locator)

            # vertical, small x tick labels
            for tick in self.axes.get_xticklabels():
                tick.set_rotation('vertical')
                tick.set_fontsize('x-small')

    def replot(self, new_sensor=None, position=None):
        with self._replot_lock:
            # if the plot is locked, and this is not a slider change,
            # just bail out
            if self._is_locked and position is None:
                return

            # check if new sensor needs to be set
            if new_sensor is not None and new_sensor is not self._sensor:
                self.log
                self.clear_plot()
                self._sensor = new_sensor
                self._initial_plot(position)
            elif self._sensor is not None:
                if len(self._sensor.get_data()) != len(self.lines):
                    # we need to replot everything, there are more sensors
                    self.clear_plot(clear_sensor=False)
                    self._initial_plot(position)
                else:
                    i = 0

                    for _uid, x, y in self._sensor.get_data():
                        self.lines[i].set_ydata(y)
                        self.lines[i].set_xdata(x)
                        i = i + 1

                    self._adjust_axes(position)
                    self._adjust_ticks()

                    self.axes.relim()
                    self.axes.autoscale_view()

            # redraw
            self.draw()

    def set_settings(self, settings):
        self.log.trace('Settings changed:\n%s' % str(settings))
        self.settings = settings
        self.replot()

    def _initial_plot(self, position):
        # prepare stuff
        self._prepare_plot()
        self._adjust_axes(position)
        self._adjust_ticks()

        # plot the first time
        self.lines = []
        uids = []
        for uid, x, y in self._sensor.get_data():
            line, = self.axes.plot(x, y,
                                   marker='o',
                                   markersize=1.5)
            self.lines.append(line)
            uids.append(uid)

        legend(uids)

        self.log.debug('Initial plot of data for %s.', self._sensor)
