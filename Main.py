'''
Created on Apr 15, 2014

@author: dimitrijer
'''
import logging
import signal
from sensei_util.log.Logger import Logger
from handlers import handlers
from ui.MainWindow import init_gui
from PyQt4.Qt import QTimer
from PyQt4 import QtGui
import sys
from sensei_util.comm.server import Server
from BackupTimer import BackupTimer


def signal_handler(signum, frame):
    logging.getLogger().debug('Signal ' + str(signum) + 'received!')
    app.exit()

if __name__ == '__main__':
    signal.signal(signal.SIGTERM, signal_handler)
    signal.signal(signal.SIGINT, signal_handler)
    global app
    global s
    app = None
    s = None
    retval = None

    Logger.init_logging(log_trace=True)
    try:

        s = Server(5001, handlers())
        s.start()

        BackupTimer().start()

        app = QtGui.QApplication(sys.argv)

        timer = QTimer()
        # Let the interpreter run each 1 second
        timer.timeout.connect(lambda: None)
        timer.start(1000)

        init_gui()

        retval = app.exec_()
        app = None
    except Exception:
        logging.getLogger().error(
            'Exception during initialization!',
            exc_info=1)
        retval = 1
    finally:

        if app is not None:
            app.quit()

        # Stop the server
        if s is not None:
            s.stop()

        sys.exit(retval)
