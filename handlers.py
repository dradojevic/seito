'''
Created on Apr 13, 2014

@author: dimitrijer
'''

from sensei_protobuf.generated.ReadingDefinition_pb2 import DebugLog, \
    SendReadingsRequest

from sensei_util.comm import Handler

from ui import MainWindow

from sensor import SensorModel
from sensor.Reading import Reading


def handlers():
    return {
        0: DebugHandler,
        1: SendReadingsHandler
    }


class DebugHandler(Handler):

    def handle(self, payload):
        message = DebugLog()
        message.ParseFromString(payload)
        self.debug("Message: %s" % message.message)


class SendReadingsHandler(Handler):

    def handle(self, payload):
        # parse proto first
        message = SendReadingsRequest()
        message.ParseFromString(payload)

        for proto in message.readings:
            SensorModel.create_sensor(proto.sensor.uid, proto.sensor.type)

            # construct a reading
            reading = Reading(proto.sensor.uid, proto.reading)

            # add reading to the model
            SensorModel.add_reading(reading, proto.sensor.type)

            # log a bit
            self.debug('Received reading for %s.', proto.sensor.uid)

        # blink led
        MainWindow.emit_data_received()
