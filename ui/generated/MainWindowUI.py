# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'MainWindow.ui'
#
# Created: Sun Jul 20 01:47:42 2014
#      by: PyQt4 UI code generator 4.10.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName(_fromUtf8("MainWindow"))
        MainWindow.resize(800, 600)
        self.centralwidget = QtGui.QWidget(MainWindow)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.verticalLayout_2 = QtGui.QVBoxLayout(self.centralwidget)
        self.verticalLayout_2.setObjectName(_fromUtf8("verticalLayout_2"))
        spacerItem = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout_2.addItem(spacerItem)
        self.horizontalScrollBar = QtGui.QScrollBar(self.centralwidget)
        self.horizontalScrollBar.setEnabled(False)
        self.horizontalScrollBar.setMaximum(0)
        self.horizontalScrollBar.setProperty("value", 0)
        self.horizontalScrollBar.setOrientation(QtCore.Qt.Horizontal)
        self.horizontalScrollBar.setObjectName(_fromUtf8("horizontalScrollBar"))
        self.verticalLayout_2.addWidget(self.horizontalScrollBar)
        self.horizontalLayout_3 = QtGui.QHBoxLayout()
        self.horizontalLayout_3.setObjectName(_fromUtf8("horizontalLayout_3"))
        self.tableView = QtGui.QTableView(self.centralwidget)
        self.tableView.setEnabled(False)
        self.tableView.setObjectName(_fromUtf8("tableView"))
        self.horizontalLayout_3.addWidget(self.tableView)
        self.verticalLayout_4 = QtGui.QVBoxLayout()
        self.verticalLayout_4.setObjectName(_fromUtf8("verticalLayout_4"))
        self.pushSave = QtGui.QPushButton(self.centralwidget)
        self.pushSave.setEnabled(False)
        self.pushSave.setObjectName(_fromUtf8("pushSave"))
        self.verticalLayout_4.addWidget(self.pushSave)
        self.pushClear = QtGui.QPushButton(self.centralwidget)
        self.pushClear.setEnabled(False)
        self.pushClear.setObjectName(_fromUtf8("pushClear"))
        self.verticalLayout_4.addWidget(self.pushClear)
        self.checkLock = QtGui.QCheckBox(self.centralwidget)
        self.checkLock.setObjectName(_fromUtf8("checkLock"))
        self.verticalLayout_4.addWidget(self.checkLock)
        self.horizontalLayout_3.addLayout(self.verticalLayout_4)
        self.verticalLayout_2.addLayout(self.horizontalLayout_3)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtGui.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 800, 20))
        self.menubar.setObjectName(_fromUtf8("menubar"))
        self.menuFile = QtGui.QMenu(self.menubar)
        self.menuFile.setObjectName(_fromUtf8("menuFile"))
        self.menuGraph = QtGui.QMenu(self.menubar)
        self.menuGraph.setObjectName(_fromUtf8("menuGraph"))
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtGui.QStatusBar(MainWindow)
        self.statusbar.setObjectName(_fromUtf8("statusbar"))
        MainWindow.setStatusBar(self.statusbar)
        self.actionExit = QtGui.QAction(MainWindow)
        self.actionExit.setObjectName(_fromUtf8("actionExit"))
        self.actionSettings = QtGui.QAction(MainWindow)
        self.actionSettings.setEnabled(False)
        self.actionSettings.setObjectName(_fromUtf8("actionSettings"))
        self.menuFile.addAction(self.actionExit)
        self.menuGraph.addAction(self.actionSettings)
        self.menubar.addAction(self.menuFile.menuAction())
        self.menubar.addAction(self.menuGraph.menuAction())

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(_translate("MainWindow", "Seito", None))
        self.pushSave.setText(_translate("MainWindow", "Save data...", None))
        self.pushClear.setText(_translate("MainWindow", "Clear", None))
        self.checkLock.setText(_translate("MainWindow", "Lock plot", None))
        self.menuFile.setTitle(_translate("MainWindow", "&File", None))
        self.menuGraph.setTitle(_translate("MainWindow", "&Plot", None))
        self.actionExit.setText(_translate("MainWindow", "E&xit", None))
        self.actionSettings.setText(_translate("MainWindow", "&Settings...", None))

