'''
Created on Apr 14, 2014

@author: dimitrijer
'''

from sensei_protobuf.generated import ReadingDefinition_pb2

from sensei_util.log import Logger

from datetime import datetime
import csv


class AbstractSensor(Logger):

    '''
    Abstract sensor class. Contains interface for providing data.
    '''

    TEMPERATURE = ReadingDefinition_pb2.TEMPERATURE
    CURRENT = ReadingDefinition_pb2.CURRENT
    '''Types of sensors.'''

    types_description = {
        TEMPERATURE: 'TP',
        CURRENT: 'AC'
    }
    '''Sensor types descriptions.'''

    @classmethod
    def type_to_str(cls, sensor_type):
        return cls.types_description[sensor_type] \
            if sensor_type in cls.types_description else None

    def __init__(self, sensor_uid, sensor_type, parent=None):
        super(AbstractSensor, self).__init__()

        self.uid = sensor_uid
        self.type = sensor_type
        self.start_timestamp = datetime.now()
        self.parent = parent

    def append(self, reading):
        '''
        Appends a reading to this sensor's data set.
        '''
        raise NotImplementedError()

    def get_max(self):
        '''
        Gets maximum value from data set.
        '''
        raise NotImplementedError()

    def get_min(self):
        '''
        Gets minimum value from data set.
        '''
        raise NotImplementedError()

    def get_avg(self):
        '''
        Gets average value of data set.
        '''
        raise NotImplementedError()

    def get_last_reading(self):
        '''
        Gets the last reading received from data set.
        '''
        raise NotImplementedError()

    def get_first_reading(self):
        '''
        Gets the last reading received from data set.
        '''
        raise NotImplementedError()

    def get_data(self):
        '''
        Returns entire data set as an array of (UID-X array-Y array) tuples
        which contain values and timestamps.
        '''
        raise NotImplementedError()

    def clear(self):
        '''
        Clears entire data set.
        '''
        raise NotImplementedError()


class Sensor(AbstractSensor):

    '''
    This class encapsulates single sensor info and its data.
    '''

    def __init__(self, sensor_uid, sensor_type, parent=None):
        super(Sensor, self).__init__(sensor_uid, sensor_type, parent)

        self._min = float('inf')
        self._max = -float('inf')
        self._avg = None
        self._sum = 0

        self._readings = []
        self._buffered_values = []
        self._buffered_timestamps = []

    def append(self, reading):
        self._readings.append(reading)

        # buffer values and times
        self._buffered_values.append(reading.data)
        self._buffered_timestamps.append(reading.timestamp)

        # recalculate stuff
        self._min = min(self._min, reading.data)
        self._max = max(self._max, reading.data)
        self._sum += reading.data
        self._avg = self._sum / len(self)

    def clear(self):
        self._readings = []
        self._buffered_values = []
        self._buffered_timestamps = []

        self._min = float('inf')
        self._max = -float('inf')
        self._avg = None
        self._sum = 0

    def save(self, filename):
        try:
            # open file
            with open(filename, 'wb') as f:

                w = csv.writer(f, dialect='excel')
                for reading in self._readings:
                    w.writerow([reading.timestamp, reading.data])

            self.debug(
                'Successfully dumped data for %s to: %s',
                self,
                filename)
        except Exception:
            self.error('Failed writing %s to file %s!', str(self), filename,
                       exc_info=1)

    def get_max(self):
        return self._max

    def get_min(self):
        return self._min

    def get_avg(self):
        return self._avg

    def get_last_reading(self):
        return self._readings[-1] if self._readings else None

    def get_first_reading(self):
        return self._readings[0] if self._readings else None

    def get_data(self):
        return [(self.uid, self._buffered_timestamps, self._buffered_values)]

    def __str__(self):
        return 'Sensor(UID:%s, %s){%d readings}' % \
            (str(self.uid), Sensor.type_to_str(self.type), len(self))

    def __len__(self):
        return len(self._readings)

    def __repr__(self):
        return self.__str__()


class CompositeSensor(AbstractSensor):

    '''
    A sensor that contains other sensors.
    '''

    def __init__(self, sensor_uid, sensor_type, parent=None):
        super(CompositeSensor, self).__init__(sensor_uid, sensor_type, parent)

        self._children = []
        self._children_by_uid = dict()

    def append(self, reading):
        raise RuntimeError(
            "Readings should not be appended to composite sensors!")

    def children(self):
        return self._children

    def add_child(self, sensor):
        '''
        Adds other sensor as this sensor's child.
        '''
        sensor.parent = self
        self._children.append(sensor)
        self._children_by_uid[sensor.uid] = sensor
        self.debug('Added child sensor: %s' % str(sensor))

    def get_max(self):
        return max(sensor.get_max() for sensor in self._children)

    def get_min(self):
        return min(sensor.get_min() for sensor in self._children)

    def get_avg(self):
        return sum(sum(sensor.get_data()[0][2])
                   for sensor in self._children) / len(self)

    def get_last_reading(self):
        last_readings = [sensor.get_last_reading()
                         for sensor in self._children]
        reading_timestamps = [reading.timestamp for reading in last_readings]
        index_of_most_recent = reading_timestamps.index(
            max(reading_timestamps))
        return last_readings[index_of_most_recent]

    def get_first_reading(self):
        first_readings = [sensor.get_first_reading()
                          for sensor in self._children]
        reading_timestamps = [reading.timestamp for reading in first_readings]
        index_of_least_recent = reading_timestamps.index(
            min(reading_timestamps))
        return first_readings[index_of_least_recent]

    def get_data(self):
        return sum((sensor.get_data() for sensor in self._children), [])

    def clear(self):
        for sensor in self._children:
            sensor.clear()

    def __len__(self):
        return sum(len(sensor) for sensor in self._children)

    def __str__(self):
        return 'CompositeSensor(UID:%s, %s)[%s]' % \
            (str(self.uid), Sensor.type_to_str(self.type), str(self._children))

    def __repr__(self):
        return self.__str__()
